package plugins.adufour.projection;

import java.util.Collections;
import java.util.List;

import org.bioimageanalysis.icy.image.projection.ProjectionAxis;
import org.bioimageanalysis.icy.image.projection.ProjectionCalculator;
import org.bioimageanalysis.icy.image.projection.ProjectionOperationType;

import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarSequence;

public class Projection extends EzPlug implements Block, EzStoppable
{
    public enum ProjectionDirection
    {
        Z, T, C, Y, X
    }

    public enum ProjectionType
    {
        MAX("Maximum"), MEAN("Average"), MED("Median"), MIN("Minimum"), STD("Standard Deviation"),
        SATSUM("Saturated Sum");

        private final String description;

        ProjectionType(String description)
        {
            this.description = description;
        }

        @Override
        public String toString()
        {
            return description;
        }
    }

    private EzVarSequence input;

    private EzVarEnum<ProjectionDirection> projectionDir;

    private EzVarEnum<ProjectionType> projectionType;

    private EzVarBoolean restrictToROI;

    private VarSequence output;

    @Override
    protected void initialize()
    {
        initCommonVars();
        restrictToROI.setToolTipText(
                "Check this option to project only the intensity data contained within the sequence ROI");

        addEzComponent(input);
        addEzComponent(projectionDir);
        addEzComponent(projectionType);
        addEzComponent(restrictToROI);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        initCommonVars();
        inputMap.add("input", input.getVariable());
        inputMap.add("projection direction", projectionDir.getVariable());
        inputMap.add("projection type", projectionType.getVariable());
        inputMap.add("restrict to ROI", restrictToROI.getVariable());
    }

    private void initCommonVars()
    {
        input = new EzVarSequence("Input");
        projectionDir = new EzVarEnum<Projection.ProjectionDirection>("Project along", ProjectionDirection.values(),
                ProjectionDirection.Z);
        projectionType = new EzVarEnum<Projection.ProjectionType>("Projection type", ProjectionType.values(),
                ProjectionType.MAX);
        restrictToROI = new EzVarBoolean("Restrict to ROI", false);
        setTimeDisplay(true);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        output = new VarSequence("projected sequence", null);
        outputMap.add("projection output", output);
    }

    @Override
    protected void execute()
    {
        if (!isHeadLess())
        {
            getUI().setProgressBarVisible(true);
            getUI().setProgressBarValue(Double.NaN);
        }

        readInput();
        try
        {
            computeCalculator();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new IcyHandledException("Error while projecting: " + e.getMessage(), e);
        }
        setOutput();
        internalClean();
    }

    private Sequence inputSequence;
    private ProjectionAxis axis;
    private ProjectionOperationType op;
    private List<ROI> rois;

    private void readInput()
    {
        inputSequence = input.getValue(true);
        axis = getAxis(projectionDir.getValue(true));
        op = getOperation(projectionType.getValue(true));
        rois = restrictToROI.getValue(true) ? inputSequence.getROIs() : Collections.emptyList();
    }

    private static ProjectionAxis getAxis(ProjectionDirection direction)
    {
        switch (direction)
        {
            case X:
                return ProjectionAxis.X;
            case Y:
                return ProjectionAxis.Y;
            case C:
                return ProjectionAxis.C;
            case Z:
                return ProjectionAxis.Z;
            case T:
                return ProjectionAxis.T;

            default:
                throw new IllegalArgumentException("" + direction);
        }
    }

    private static ProjectionOperationType getOperation(ProjectionType type)
    {
        switch (type)
        {
            case MAX:
                return ProjectionOperationType.MAX;
            case MEAN:
                return ProjectionOperationType.MEAN;
            case MED:
                return ProjectionOperationType.MED;
            case MIN:
                return ProjectionOperationType.MIN;
            case SATSUM:
                return ProjectionOperationType.SATSUM;
            case STD:
                return ProjectionOperationType.STD;

            default:
                throw new IllegalArgumentException("" + type);
        }
    }

    private Sequence resultSequence;

    private void computeCalculator() throws Exception
    {
        ProjectionCalculator calculator = new ProjectionCalculator.Builder(inputSequence).axis(axis).operation(op)
                .addRois(rois).build();
        if (!isHeadLess())
        {
            calculator.addProgressListener(this::onProgress);
        }

        try
        {
            resultSequence = calculator.call();
        }
        finally
        {
            if (!isHeadLess())
            {
                calculator.removeProgressListener(this::onProgress);
            }
        }
    }

    private void onProgress(double progress, String message)
    {
        if (!isHeadLess())
        {
            getUI().setProgressBarValue(progress);
            getUI().setProgressBarMessage(message);
        }
    }

    private void setOutput()
    {
        if (isHeadLess())
            output.setValue(resultSequence);
        else
            addSequence(resultSequence);
    }

    private void internalClean()
    {
        if (!isHeadLess())
            getUI().setProgressBarVisible(false);

        inputSequence = null;
        axis = null;
        op = null;
        rois = null;
        resultSequence = null;
    }

    @Override
    public void clean()
    {
    }

    /**
     * Performs a Z projection of the input sequence using the specified algorithm. If the sequence
     * is already 2D, then a copy of the sequence is returned
     * 
     * @param in
     *        the sequence to project
     * @param projection
     *        the type of projection to perform (see {@link ProjectionType} enumeration)
     * @return the projected sequence
     * @throws Exception
     *         If the projection cannot be correctly done.
     * @see ProjectionCalculator
     */
    public static Sequence zProjection(final Sequence in, final ProjectionType projection) throws Exception
    {
        return zProjection(in, projection, true, false);
    }

    /**
     * Performs a Z projection of the input sequence using the specified algorithm. If the sequence
     * is already 2D, then a copy of the sequence is returned
     * 
     * @param in
     *        the sequence to project
     * @param projection
     *        the type of projection to perform (see {@link ProjectionType} enumeration)
     * @param multiThread
     *        deprecated (there is not reason to not use it)
     * @return the projected sequence
     * @throws Exception
     *         If the projection cannot be correctly done.
     * @see ProjectionCalculator
     */
    public static Sequence zProjection(final Sequence in, final ProjectionType projection, boolean multiThread)
            throws Exception
    {
        return zProjection(in, projection, multiThread, false);
    }

    /**
     * Performs a Z projection of the input sequence using the specified algorithm. If the sequence
     * is already 2D, then a copy of the sequence is returned
     * 
     * @param in
     *        the sequence to project
     * @param projection
     *        the type of projection to perform (see {@link ProjectionType} enumeration)
     * @param multiThread
     *        deprecated (there is not reason to not use it)
     * @param restrictToROI
     *        <code>true</code> projects only data located within the sequence ROI,
     *        <code>false</code> projects the entire data set
     * @return the projected sequence
     * @throws Exception
     *         If the projection cannot be correctly done.
     * @see ProjectionCalculator
     */
    public static Sequence zProjection(final Sequence in, final ProjectionType projection, boolean multiThread,
            boolean restrictToROI) throws Exception
    {
        List<ROI> rois = restrictToROI ? in.getROIs() : Collections.emptyList();
        ProjectionCalculator calculator = new ProjectionCalculator.Builder(in).axis(ProjectionAxis.Z)
                .operation(getOperation(projection)).addRois(rois).build();
        return calculator.call();
    }

    /**
     * Performs a T projection of the input sequence using the specified algorithm. If the sequence
     * has only one time point, then a copy of the sequence is returned
     * 
     * @param in
     *        the sequence to project
     * @param projection
     *        the type of projection to perform (see {@link ProjectionType} enumeration)
     * @param multiThread
     *        true if the process should be multi-threaded
     * @return the projected sequence
     * @throws Exception
     *         If the projection cannot be correctly done.
     * @see ProjectionCalculator
     */
    public static Sequence tProjection(final Sequence in, final ProjectionType projection, boolean multiThread)
            throws Exception
    {
        return tProjection(in, projection, multiThread, false);
    }

    /**
     * Performs a T projection of the input sequence using the specified algorithm. If the sequence
     * has only one time point, then a copy of the sequence is returned
     * 
     * @param in
     *        the sequence to project
     * @param projection
     *        the type of projection to perform (see {@link ProjectionType} enumeration)
     * @param multiThread
     *        true if the process should be multi-threaded
     * @param restrictToROI
     *        <code>true</code> projects only data located within the sequence ROI,
     *        <code>false</code> projects the entire data set
     * @return the projected sequence
     * @throws Exception
     *         If the projection cannot be correctly done.
     * @see ProjectionCalculator
     */
    public static Sequence tProjection(final Sequence in, final ProjectionType projection, boolean multiThread,
            boolean restrictToROI) throws Exception
    {
        List<ROI> rois = restrictToROI ? in.getROIs() : Collections.emptyList();
        ProjectionCalculator calculator = new ProjectionCalculator.Builder(in).axis(ProjectionAxis.T)
                .operation(getOperation(projection)).addRois(rois).build();
        return calculator.call();
    }

    /**
     * Main method used for testing.
     * 
     * @param args
     *        Input arguments.
     */
    public static void main(String[] args)
    {
        // start icy
        Icy.main(args);

        // then start plugin
        PluginLauncher.start(PluginLoader.getPlugin(Projection.class.getName()));
    }
}
