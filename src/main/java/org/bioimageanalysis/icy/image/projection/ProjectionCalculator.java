package org.bioimageanalysis.icy.image.projection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

import org.bioimageanalysis.icy.image.projection.util.MessageProgressListener;

import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageCursor;
import icy.math.ArrayMath;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.VolumetricImage;
import icy.type.DataType;
import icy.util.OMEUtil;

/**
 * Calculates the projection of a sequence along the specified angle and using the specified operation. The projection can be applied to a given set of ROIs,
 * only pixels contained in the ROIs will be taken into account to compute the projection.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class ProjectionCalculator implements Callable<Sequence>
{

    /**
     * Builder class for {@link ProjectionCalculator}s. This allows users to specify the sequence, ROIs, axis and operation before creating the calculator.
     * 
     * @author Daniel Felipe Gonzalez Obando
     */
    public static class Builder
    {
        private Sequence s;
        private List<ROI> rois;
        private ProjectionAxis axis;
        private ProjectionOperationType op;

        /**
         * Creates a builder instance with the target sequence.
         * By default, the ROI list is set to empty, the projection axis is set to {@link ProjectionAxis#Z} and the operation is set to
         * {@link ProjectionOperationType#MAX}.
         * 
         * @param s
         *        Target sequence for the projection.
         */
        public Builder(Sequence s)
        {
            if (s == null || s.isEmpty())
                throw new IllegalArgumentException("Input sequence is null or empty.");

            this.s = s;
            this.rois = new ArrayList<ROI>();
            this.axis = ProjectionAxis.Z;
            this.op = ProjectionOperationType.MAX;
        }

        /**
         * Adds a ROI to the projection constraints.
         * 
         * @param roi
         *        The ROI.
         * @return This builder instance once the ROI is added.
         */
        public Builder addRoi(ROI roi)
        {
            rois.add(roi);
            return this;
        }

        /**
         * Adds a collection of ROIs to the projection constraints.
         * 
         * @param rois
         *        The collection of ROIs to add.
         * @return This builder instance once the ROIs have been added.
         */
        public Builder addRois(Collection<? extends ROI> rois)
        {
            this.rois.addAll(rois);
            return this;
        }

        /**
         * Sets the projection axis.
         * If the argument is null the axis is set to {@link ProjectionAxis#Z}.
         * 
         * @param axis
         *        Axis.
         * @return This builder instance after setting the axis.
         */
        public Builder axis(ProjectionAxis axis)
        {
            if (axis == null)
                this.axis = ProjectionAxis.Z;
            else
                this.axis = axis;

            return this;
        }

        /**
         * Sets the operation used during the projection.
         * If the argument is null, then the operation is set to {@link ProjectionOperationType#MAX}
         * 
         * @param op
         *        Operation to apply.
         * @return This builder instance after setting the operation.
         */
        public Builder operation(ProjectionOperationType op)
        {
            if (op == null)
                this.op = ProjectionOperationType.MAX;
            else
                this.op = op;

            return this;
        }

        /**
         * Builds the calculator using the parameters from this instance.
         * 
         * @return The projection calculator ready to be called.
         */
        public ProjectionCalculator build()
        {
            ProjectionCalculator calculator = new ProjectionCalculator();
            calculator.seq = s;
            calculator.rois = rois;
            calculator.axis = axis;
            calculator.op = op;
            return calculator;

        }
    }

    private Sequence seq;
    private List<ROI> rois;
    private ProjectionAxis axis;
    private ProjectionOperationType op;
    private List<MessageProgressListener> progressListeners;
    private boolean computed;

    private ProjectionCalculator()
    {
        progressListeners = new ArrayList<>();
        computed = false;
    }

    /**
     * Adds a listener for progress events.
     * 
     * @param listener
     *        Progress event listener
     */
    public void addProgressListener(MessageProgressListener listener)
    {
        progressListeners.add(listener);
    }

    /**
     * Removes a listener for progress events.
     * 
     * @param listener
     *        Progress event listener
     */
    public void removeProgressListener(MessageProgressListener listener)
    {
        progressListeners.remove(listener);
    }

    /**
     * Starts the projection if the calculator has not already computed the result.
     */
    @Override
    public Sequence call() throws Exception
    {
        try
        {
            if (!computed)
            {
                createResultSequence();
                result.beginUpdate();
                computeProjection();
                result.endUpdate();
                computed = true;
            }
            return getResultSequence();
        }
        finally
        {
            notifyProgress(1, "Projection finished");
        }
    }

    private Sequence result;

    private void createResultSequence() throws InterruptedException
    {
        result = new Sequence(OMEUtil.createOMEXMLMetadata(seq.getOMEXMLMetadata()),
                op + " " + axis + "-projection of " + seq.getName());

        final int width = axis == ProjectionAxis.X ? 1 : seq.getSizeX();
        final int height = axis == ProjectionAxis.Y ? 1 : seq.getSizeY();
        final int depth = axis == ProjectionAxis.Z ? 1 : seq.getSizeZ();
        final int frames = axis == ProjectionAxis.T ? 1 : seq.getSizeT();
        final int channels = axis == ProjectionAxis.C ? 1 : seq.getSizeC();
        final DataType dataType = seq.getDataType_();

        for (int t = 0; t < frames; t++)
        {
            VolumetricImage vol = new VolumetricImage();
            for (int z = 0; z < depth; z++)
            {
                if (Thread.interrupted())
                    throw new InterruptedException();
                vol.setImage(z, new IcyBufferedImage(width, height, channels, dataType));
            }
            result.addVolumetricImage(t, vol);
            notifyProgress(Math.max(0.01, t * (0.1 / frames)), "Creating result sequence t=" + t);
        }

        if (axis != ProjectionAxis.C)
        {
            for (int c = 0; c < seq.getSizeC(); c++)
                result.getColorModel().setColorMap(c, seq.getColorMap(c), true);
        }
        notifyProgress(0.01, "Result sequence instatiated");
    }

    private void computeProjection() throws InterruptedException, ExecutionException
    {

        switch (axis)
        {
            case X:
                startProjectionOnPlane();
                break;
            case Y:
                startProjectionOnPlane();
                break;
            case Z:
                startZProjection();
                break;
            case T:
                startTProjection();
                break;
            case C:
                startProjectionOnPlane();
                break;
        }
        notifyProgress(1, axis + "-projection done");
    }

    private void startProjectionOnPlane() throws InterruptedException, ExecutionException
    {
        final int frames = seq.getSizeT();
        final int depth = seq.getSizeZ();
        final int channels = seq.getSizeC();
        final int height = seq.getSizeY();
        final int width = seq.getSizeX();

        final IcyBufferedImage[] inputPlanes = new IcyBufferedImage[frames * depth];
        final IcyBufferedImage[] outputPlanes = new IcyBufferedImage[frames * depth];
        int t, z, tOff;
        for (t = 0; t < frames; t++)
        {
            tOff = t * depth;
            for (z = 0; z < depth; z++)
            {
                inputPlanes[tOff + z] = seq.getImage(t, z);
                outputPlanes[tOff + z] = result.getImage(t, z);
            }
        }

        ForkJoinPool generalTaskPool = (ForkJoinPool) Executors
                .newWorkStealingPool(Math.max(1, Runtime.getRuntime().availableProcessors() - 1));

        List<Future<IcyBufferedImage>> futures = new ArrayList<>(frames * depth);
        for (t = 0; t < frames; t++)
        {
            tOff = t * depth;
            for (z = 0; z < depth; z++)
            {
                Callable<IcyBufferedImage> task;
                switch (axis)
                {
                    case X:
                        task = getImageXProjectionTask(inputPlanes[tOff + z], outputPlanes[tOff + z], t, z, channels,
                                height, width);
                        break;
                    case Y:
                        task = getImageYProjectionTask(inputPlanes[tOff + z], outputPlanes[tOff + z], t, z, channels,
                                height, width);
                        break;
                    case C:
                        task = getImageCProjectionTask(inputPlanes[tOff + z], outputPlanes[tOff + z], t, z, channels,
                                height, width);
                        break;
                    default:
                        throw new IllegalArgumentException("Wrong axis parameter");
                }
                futures.add(generalTaskPool.submit(task));
            }
        }

        generalTaskPool.shutdown();

        int pos;
        for (pos = 0, t = 0; t < frames; t++)
        {
            for (z = 0; z < depth; z++)
            {
                try
                {
                    futures.get(pos++).get();
                }
                catch (InterruptedException | ExecutionException e)
                {
                    generalTaskPool.shutdownNow();
                    throw e;
                }
            }
            notifyProgress(0.01 + 0.99 * ((double) t / frames), axis + "-projection: Processed t=" + t);
        }
    }

    private Callable<IcyBufferedImage> getImageXProjectionTask(IcyBufferedImage inputPlane,
            IcyBufferedImage outputPlane, final int t, final int z, final int channels, final int height,
            final int width)
    {
        return () -> {
            int channel, line, column, elementsCount;
            double[] elements = new double[width];

            IcyBufferedImageCursor inputCursor = new IcyBufferedImageCursor(inputPlane);
            IcyBufferedImageCursor outputCursor = new IcyBufferedImageCursor(outputPlane);
            for (channel = 0; channel < channels; channel++)
            {
                for (line = 0; line < height; line++)
                {
                    elementsCount = 0;
                    for (column = 0; column < width; column++)
                    {
                        if (rois.isEmpty())
                        {
                            elements[elementsCount++] = inputCursor.get(column, line, channel);
                        }
                        else
                        {
                            for (ROI roi : rois)
                            {
                                if (roi.contains(column, line, z, t, channel))
                                {
                                    elements[elementsCount++] = inputCursor.get(column, line, channel);
                                    break;
                                }
                            }
                        }
                    }
                    if (elementsCount == 0)
                    {
                        outputCursor.setSafe(0, line, channel, 0d);
                    }
                    else
                    {
                        outputCursor.setSafe(0, line, channel, computeResVal(Arrays.copyOf(elements, elementsCount)));
                    }

                }
            }
            outputCursor.commitChanges();
            return outputPlane;
        };
    }

    private Callable<IcyBufferedImage> getImageYProjectionTask(IcyBufferedImage inputPlane,
            IcyBufferedImage outputPlane, final int t, final int z, final int channels, final int height,
            final int width)
    {
        return () -> {
            int channel, line, column, elementsCount;
            double[] elements = new double[height];

            IcyBufferedImageCursor inputCursor = new IcyBufferedImageCursor(inputPlane);
            IcyBufferedImageCursor outputCursor = new IcyBufferedImageCursor(outputPlane);
            for (channel = 0; channel < channels; channel++)
            {
                for (column = 0; column < width; column++)
                {
                    elementsCount = 0;

                    for (line = 0; line < height; line++)
                    {
                        if (rois.isEmpty())
                        {
                            elements[elementsCount++] = inputCursor.get(column, line, channel);
                        }
                        else
                        {
                            for (ROI roi : rois)
                            {
                                if (roi.contains(column, line, z, t, channel))
                                {
                                    elements[elementsCount++] = inputCursor.get(column, line, channel);
                                    break;
                                }
                            }
                        }
                    }
                    if (elementsCount == 0)
                    {
                        outputCursor.setSafe(column, 0, channel, 0d);
                    }
                    else
                    {
                        outputCursor.setSafe(column, 0, channel, computeResVal(Arrays.copyOf(elements, elementsCount)));
                    }

                }
            }
            outputCursor.commitChanges();
            return outputPlane;
        };
    }

    private Callable<IcyBufferedImage> getImageCProjectionTask(IcyBufferedImage inputPlane,
            IcyBufferedImage outputPlane, int t, int z, int channels, int height, int width)
    {
        return () -> {
            int channel, line, column, elementsCount;
            double[] elements = new double[height];

            IcyBufferedImageCursor[] inputCursors = new IcyBufferedImageCursor[channels];
            IcyBufferedImageCursor outputCursor = new IcyBufferedImageCursor(outputPlane);
            for (channel = 0; channel < channels; channel++)
            {
                inputCursors[channel] = new IcyBufferedImageCursor(inputPlane);
            }

            for (column = 0; column < width; column++)
            {
                for (line = 0; line < height; line++)
                {
                    elementsCount = 0;

                    for (channel = 0; channel < channels; channel++)
                    {
                        if (rois.isEmpty())
                        {
                            elements[elementsCount++] = inputCursors[channel].get(column, line, channel);
                        }
                        else
                        {
                            for (ROI roi : rois)
                            {
                                if (roi.contains(column, line, z, t, channel))
                                {
                                    elements[elementsCount++] = inputCursors[channel].get(column, line, channel);
                                    break;
                                }
                            }
                        }
                    }

                    if (elementsCount == 0)
                    {
                        outputCursor.setSafe(column, line, 0, 0d);
                    }
                    else
                    {
                        outputCursor.setSafe(column, line, 0, computeResVal(Arrays.copyOf(elements, elementsCount)));
                    }
                }

            }

            outputCursor.commitChanges();

            return outputPlane;
        };

    }

    private void startZProjection() throws InterruptedException, ExecutionException
    {
        final int width = seq.getSizeX();
        final int height = seq.getSizeY();
        final int depth = seq.getSizeZ();
        final int frames = seq.getSizeT();
        final int channels = seq.getSizeC();

        ForkJoinPool generalTaskPool = (ForkJoinPool) Executors
                .newWorkStealingPool(Math.max(1, Runtime.getRuntime().availableProcessors() / 2));
        ForkJoinPool volumeTaskPool = (ForkJoinPool) Executors
                .newWorkStealingPool(Math.max(1, Runtime.getRuntime().availableProcessors() - 1));
        int t, c;
        VolumetricImage inputVolume, resultVolume;
        List<Future<VolumetricImage>> futures = new ArrayList<>(frames * depth);
        for (t = 0; t < frames; t++)
        {
            inputVolume = seq.getVolumetricImage(t);
            resultVolume = result.getVolumetricImage(t);
            for (c = 0; c < channels; c++)
            {
                futures.add(generalTaskPool.submit(getImageZProjectionTask(t, c, inputVolume, resultVolume, channels,
                        height, width, depth, volumeTaskPool)));

            }
        }

        generalTaskPool.shutdown();

        try
        {
            int pos;
            for (pos = 0, t = 0; t < frames; t++)
            {
                for (c = 0; c < channels; c++)
                {
                    futures.get(pos++).get();
                }
                notifyProgress(0.01 + 0.99 * ((double) t / frames), axis + "-projection: Processed t=" + t);
            }
        }
        catch (InterruptedException | ExecutionException e)
        {
            generalTaskPool.shutdownNow();
            throw e;
        }
        finally
        {
            volumeTaskPool.shutdown();
        }

    }

    private Callable<VolumetricImage> getImageZProjectionTask(int t, int c, VolumetricImage inputVolume,
            VolumetricImage resultVolume, int channels, int height, int width, int depth, ForkJoinPool volumeTaskPool)
    {
        return () -> {
            int slice, line, column;

            IcyBufferedImageCursor[] inputCursors = new IcyBufferedImageCursor[depth];
            IcyBufferedImageCursor outputCursor = new IcyBufferedImageCursor(resultVolume.getImage(0));
            for (slice = 0; slice < depth; slice++)
            {
                inputCursors[slice] = new IcyBufferedImageCursor(inputVolume.getImage(slice));
            }

            ArrayList<Future<double[]>> futureLines = new ArrayList<>(width);
            for (line = 0; line < height; line++)
            {
                final int y = line;
                futureLines.add(volumeTaskPool.submit(() -> {
                    int x, elemCount, z;
                    double[] lineElements = new double[width], elements = new double[depth];

                    for (x = 0; x < width; x++)
                    {
                        elemCount = 0;
                        for (z = 0; z < depth; z++)
                        {
                            if (rois.isEmpty())
                            {
                                elements[elemCount++] = inputCursors[z].get(x, y, c);
                            }
                            else
                            {
                                for (ROI roi : rois)
                                {
                                    if (roi.contains(x, y, z, t, c))
                                    {
                                        elements[elemCount++] = inputCursors[z].get(x, y, c);
                                        break;
                                    }
                                }
                            }
                        }

                        if (elemCount == 0)
                        {
                            lineElements[x] = 0d;
                        }
                        else
                        {
                            lineElements[x] = computeResVal(Arrays.copyOf(elements, elemCount));
                        }
                    }

                    return lineElements;
                }));
            }

            try
            {
                for (line = 0; line < height; line++)
                {
                    double[] lineElements = futureLines.get(line).get();
                    for (column = 0; column < width; column++)
                    {
                        outputCursor.setSafe(column, line, c, lineElements[column]);
                    }
                }
            }
            finally
            {
                outputCursor.commitChanges();
            }

            return resultVolume;
        };

    }

    private void startTProjection() throws InterruptedException, ExecutionException
    {
        final int width = seq.getSizeX();
        final int height = seq.getSizeY();
        final int depth = seq.getSizeZ();
        final int frames = seq.getSizeT();
        final int channels = seq.getSizeC();

        ForkJoinPool generalTaskPool = (ForkJoinPool) Executors
                .newWorkStealingPool(Math.max(1, Runtime.getRuntime().availableProcessors() / 2));
        ForkJoinPool temporalTaskPool = (ForkJoinPool) Executors
                .newWorkStealingPool(Math.max(1, Runtime.getRuntime().availableProcessors() - 1));
        int z, c;
        Sequence inputS, resultS;
        List<Future<Sequence>> futures = new ArrayList<>(depth);
        for (z = 0; z < depth; z++)
        {
            for (c = 0; c < channels; c++)
            {
                inputS = seq;
                resultS = result;
                {
                    futures.add(generalTaskPool.submit(getImageTProjectionTask(z, c, inputS, resultS, channels, height,
                            width, depth, frames, temporalTaskPool)));
                }
            }
        }

        generalTaskPool.shutdown();

        try
        {
            int pos;
            for (pos = 0, z = 0; z < depth; z++)
            {
                for (c = 0; c < channels; c++)
                {
                    futures.get(pos++).get();
                }
                notifyProgress(0.01 + 0.99 * ((double) z / depth), axis + "-projection: Processed z=" + z);
            }
        }
        catch (InterruptedException | ExecutionException e)
        {
            generalTaskPool.shutdownNow();
            throw e;
        }
        finally
        {
            temporalTaskPool.shutdown();
        }
    }

    private Callable<Sequence> getImageTProjectionTask(int z, int c, Sequence inputS, Sequence resultS, int channels,
            int height, int width, int depth, int frames, ForkJoinPool temporalTaskPool)
    {
        return () -> {
            int frame, line, column;
            IcyBufferedImageCursor[] inputCursors = new IcyBufferedImageCursor[frames];
            IcyBufferedImageCursor outputCursor = new IcyBufferedImageCursor(resultS.getImage(0, z));
            for (frame = 0; frame < frames; frame++)
            {
                inputCursors[frame] = new IcyBufferedImageCursor(inputS.getImage(frame, z));
            }

            List<Future<double[]>> futureLines = new ArrayList<>(height);
            for (line = 0; line < height; line++)
            {
                final int y = line;
                futureLines.add(temporalTaskPool.submit(() -> {
                    int x, elemCount, t;
                    double[] lineElements = new double[width], elements = new double[frames];

                    for (x = 0; x < width; x++)
                    {
                        elemCount = 0;
                        for (t = 0; t < frames; t++)
                        {
                            if (rois.isEmpty())
                            {
                                elements[elemCount++] = inputCursors[t].get(x, y, c);
                            }
                            else
                            {
                                for (ROI roi : rois)
                                {
                                    if (roi.contains(x, y, z, t, c))
                                    {
                                        elements[elemCount++] = inputCursors[t].get(x, y, c);
                                        break;
                                    }
                                }
                            }
                        }

                        if (elemCount == 0)
                        {
                            lineElements[x] = 0d;
                        }
                        else
                        {
                            lineElements[x] = computeResVal(Arrays.copyOf(elements, elemCount));
                        }
                    }
                    return lineElements;
                }));
            }

            try
            {
                for (line = 0; line < height; line++)
                {
                    double[] lineElements = futureLines.get(line).get();
                    for (column = 0; column < width; column++)
                    {
                        outputCursor.setSafe(column, line, c, lineElements[column]);
                    }
                }
            }
            finally
            {
                outputCursor.commitChanges();
            }

            return resultS;
        };
    }

    private double computeResVal(double[] elements)
    {
        switch (op)
        {
            case MAX:
                return ArrayMath.max(elements);
            case MEAN:
                return ArrayMath.mean(elements);
            case MED:
                if (elements.length == 1)
                    return elements[0];
                else
                    return ArrayMath.median(elements, false);
            case MIN:
                return ArrayMath.min(elements);
            case SATSUM:
                return ArrayMath.sum(elements);
            case STD:
                return ArrayMath.std(elements, true);
        }
        return 0;
    }

    private void notifyProgress(double progress, String message)
    {
        progressListeners.forEach(l -> l.onProgress(progress, message));
    }

    private Sequence getResultSequence()
    {
        return result;
    }

    public void reset()
    {
        result = null;
        computed = false;
    }
}
