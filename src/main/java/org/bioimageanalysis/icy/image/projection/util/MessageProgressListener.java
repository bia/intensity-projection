/**
 * 
 */
package org.bioimageanalysis.icy.image.projection.util;

/**
 * A Progress event listener that handles events associating a value and a message to them.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public interface MessageProgressListener
{
    /**
     * A method handling progress events with a value and a message.
     * 
     * @param progress
     *        The progress value from 0.0 to 1.0. {@link Double#NaN} value should be taken as no change and -1.0 as indeterminate progress.
     * @param message
     *        The event message. A null value should be taken as no change on the message.
     */
    void onProgress(double progress, String message);
}
