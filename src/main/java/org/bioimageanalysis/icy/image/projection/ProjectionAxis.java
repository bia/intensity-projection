package org.bioimageanalysis.icy.image.projection;

/**
 * Indicates the direction on which the projection should be performed.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public enum ProjectionAxis
{
    X("x"), Y("y"), Z("z"), T("t"), C("c");

    private String name;

    ProjectionAxis(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
