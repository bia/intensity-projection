/**
 * 
 */
package org.bioimageanalysis.icy.image.projection;

/**
 * Represents the operation to be applied when performing an intensity projection on a sequence.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public enum ProjectionOperationType
{
    MAX("Maximum"), MEAN("Average"), MED("Median"), MIN("Minimum"), STD("Standard Deviation"), SATSUM("Saturated Sum");

    private final String description;

    ProjectionOperationType(String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return description;
    }
}
